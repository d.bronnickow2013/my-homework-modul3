// Copyright Epic Games, Inc. All Rights Reserved.

#include "Unreal_git.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Unreal_git, "Unreal_git" );
