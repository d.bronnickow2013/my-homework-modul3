// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unreal_gitGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_GIT_API AUnreal_gitGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
